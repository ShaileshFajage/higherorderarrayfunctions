

function find(elements, cb) {
    
    for(let i=0;i<elements.length;i++)
    {
        if(cb(elements[i])==true)
        {
            return elements[i];
        }
    }


}

function cb(num) {
    
    if(num>2)
    {
        return true;
    }
}

module.exports = 
{
    find,
    cb
}