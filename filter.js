

function filter(elements, cb) {

    const resultArr = [];

    for(let i=0;i<elements.length;i++)
    {
        if(cb(elements[i]))
        {
            resultArr.push(elements[i]);
        }
    }

    return resultArr;
}


function cb(x) {
    
    return x>2 ? true : false;
}


module.exports = {
    filter,
    cb
}
