
const items = [1, 2, 3, 4, 5, 5];

function reduce(elements, cb, startingValue)
{
    sum = cb(elements, startingValue);

    return sum;
}

function cb(elements, sum) {
    
    for(let i=0;i<elements.length;i++)
    {
        sum+=elements[i];
    }

    return sum;
}

module.exports = 
{
    reduce,
    cb
}