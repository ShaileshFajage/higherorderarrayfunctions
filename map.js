

function map(elements, cb) {

    let result = [];
    
    for(i=0;i<elements.length;i++)
    {
        result.push(cb(elements[i]));
    }

    return result;
}

function cb(num) {
    
    return num*2;

}
    

