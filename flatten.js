
const nestedArray = [1, [2], [[3]], [[[4]]]];

function flatten(elements) {

    const result = [];
  
    toFlat(elements);
  
    function toFlat(elements) {
      for (let i = 0; i < elements.length; i++) {
        if (Array.isArray(elements[i])) {
          toFlat(elements[i]);
        } else {
          result.push(elements[i]);
        }
      }
    }
  
    return result;
  }

  module.exports = flatten;


